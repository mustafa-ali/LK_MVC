<a href="<?= BASEURL . 'index.php/?r=home/insertbarang' ?>" type="button" class="btn btn-primary my-3">Tambah Barang</a>
<table class="table">
    <tr class="thead table-dark">
        <th>ID</th>
        <th>Nama Barang</th>
        <th>QTY</th>
        <th></th>
        <th></th>
    </tr>
    <?php
    foreach ($data as $item) : ?>
        <tr scope="row">
            <td><?= $item['id']; ?></td>
            <td><?= $item['nama']; ?></td>
            <td><span class="badge bg-<?= $item['qty'] > 50 ? 'success' : 'danger' ?>"><?= $item['qty'] ?></span></td>
            <td><a href="<?= BASEURL . 'index.php?r=home/updatebarang/' . $item['id'] ?>" class="btn badge text-primary">Update</a></td>
            <td><a href="<?= BASEURL . 'index.php?r=home/deletebarang/' . $item['id'] ?>" class="btn badge text-danger" onClick="return confirm('apakah anda yakin untuk menghapus data?')">Delete</a></td>
        </tr>
    <?php endforeach ?>
</table>