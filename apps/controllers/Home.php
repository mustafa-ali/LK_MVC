<?php

class Home extends Controller
{

    private $dt;
    private $df;
    public function __construct()
    {
        $this->dt = $this->loadModel('barang');
        $this->df = $this->loadModel('daftarBarang');
    }

    public function index()
    {
    }

    public function home($data1, $data2)
    {
        echo $data1 . $data2;
    }

    public function lihatData($id)
    {
        $data = $this->df->getDataById($id);

        $this->loadView('templates/header', ['title' => 'Detail Barang']);
        $this->loadView('home/detailBarang', $data);
        $this->loadView('templates/footer');
    }

    public function listBarang()
    {
        $data = $this->df->getDataAll();

        $this->loadView('templates/header', ['title' => 'List Barang']);
        $this->loadView('home/listBarang', $data);
        $this->loadView('templates/footer');
    }

    public function insertBarang()
    {
        if (!empty($_POST)) {
            if ($this->df->tambahBarang($_POST)) {
                header('Location: ' . BASEURL . 'index.php?r=home/listbarang');
            };
        }

        $this->loadView('templates/header', ['title' => 'Insert Barang']);
        $this->loadView('home/insert');
        $this->loadView('templates/footer');
    }

    public function updateBarang($id)
    {
        $data = $this->df->getDataById($id);

        if (!empty($_POST)) {
            if ($this->df->updateBarang($_POST)) {
                header('Location: ' . BASEURL . 'index.php?r=home/listbarang');
            };
        }

        $this->loadView('templates/header', ['title' => 'Update Barang']);
        $this->loadView('home/update', $data);
        $this->loadView('templates/footer');
    }

    public function deleteBarang($id)
    {

        $data = $this->df->getDataById($id);


        if ($this->df->hapusBarang($id)) {
            header('Location: ' . BASEURL . 'index.php?r=home/listbarang');
            exit;
        }
    }
}
